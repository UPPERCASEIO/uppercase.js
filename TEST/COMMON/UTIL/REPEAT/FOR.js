FOR({
	start : 5,
	end : 10
}, function(i) {
	console.log(i);
});

FOR({
	start : 1,
	limit : 5
}, function(i) {
	console.log(i);
});

FOR({
	start : 3,
	end : 1
}, function(i) {
	console.log(i);
});
