var
// data
data = {
	name : 'Yong Jae Sim',
	age : 27,
	country : 'Korea',
	now : new Date()
},

// data str
dataStr = STRINGIFY(data);

console.log(data);
console.log(dataStr);
