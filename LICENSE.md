UPPERCASE.JS LICENSE
====================
UPPERCASE.JS is a software solution developed by BTNcafe, and UPPERCASE.JS 1.3 or later version follows software license as below:

- Everyone is permitted to copy and distribute copies of UPPERCASE.JS but redistribution of modified source codes is prohibited.
- UPPERCASE.JS source codes are copyrighted and protected under patent laws.
- UPPERCASE.JS is a registered trademark of BTNcafe.

Copyright ⓒ 2014 BTNcafe & Hanul.

contact@btncafe.com / hanul@hanul.me

Software Intellectual Property Right and License
----------------------------------
Softwares are protected by Copyrights, Patent Rights, Business Secret Acts, and Trademark Rights.

Copyright : A copyright for a computer program emerges when a programmer develops a software and its copyright is granted to the programmer or a company of which.  

Patent Right : A patent is to protect inventions which is implemented by a hardware or a software. A patent right is not given automatically but it can be when legally applied for a patent followed by adequate evaluation and processes. In order to implement patent technologies, a patentee's permission is required. A patentee can prevent other people from producing or to selling products that utilize the corresponding patent. As a patent is a specific method, in order for a software that employs patent implementation, although that languages used are different, a stated consent by the patentee is neccessary and it also applies to OpenSource softwares and ClosedSource softwares.

Business Secret : A business secret is a secret that is not open to public but closed and possesses independent economic values obtained by considerable efforts, and it includes manufacturing processes, sales, and other useful technical/business activities. These busness secrets are under protection by "Unfair Competition Preention and Trade Seret Protection Act". It's strongly against laws to obtain a business secret by improper ways or to leaks out the secrets to other people when responsibility to maintain the secrets is mandatory.

Trademark : A trademark right protects related products, services, and any terms used for marketings. In addition, a trademark plays an role in differentiating my products from others'.

With intellectual property rights as above, a right holder carries exclusive rights for a software and in principle, only the right holder can use, copy, distribute and modify the software. However, for some situations, the right holer can grant a right to the third party to use the right upon certain conditions, such right is calle a license. For that reason, a license differs from sales, which is to buy and sell products, so still intellectual property rights belong to the right holder. Only part of the right to use the software is granted to the third party. In general, a license of proprietary software companies, such as Microsoft and Oracle, only allows a right for 'usage' when a customer makes a payment to the company. Thus, to copy, distribute, and modify a software without any permissions from a company is illegal and a violation against license usages.
